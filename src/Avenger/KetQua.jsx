import React, { Component } from "react";
import { connect } from "react-redux";

 class KetQua extends Component {
  render() {
    return (
      <div className="text-center">
        <div className=" text-danger display-4"><span className="my-2">{this.props.ketQua}</span></div>
        <div className=" text-white display-4">
          Số Lần Thắng: <span className="text-success">{this.props.soBanThang}</span>
        </div>
        <div className=" text-white display-4">
          Số Lần Chơi: <span className="text-warning">{this.props.soBanChoi}</span>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return{
    soBanChoi: state.avengerProducer.soBanChoi,
    soBanThang: state.avengerProducer.soBanThang,
    ketQua :  state.avengerProducer.ketQua,
  } 
}
export default connect(mapStateToProps,null)(KetQua)
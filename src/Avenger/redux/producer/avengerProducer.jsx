let initialState = {
  mangDatCuoc: [
    { ma: "keo", hinhAnh: "./imgg/keo.png", datCuoc: true },
    { ma: "bua", hinhAnh: "./imgg/bua.png", datCuoc: false },
    { ma: "bao", hinhAnh: "./imgg/bao.png", datCuoc: false },
  ],
  ketQua: "Love You 3000!",
  soBanThang: 0,
  soBanChoi: 0,
  computer: { ma: "keo", hinhAnh: "./imgg/keo.png" },
};

export const avengerProducer = (state = initialState, action) => {
  switch (action.type) {
    case "CHON_KEO_BUA_BAO": {
      let cloneMAngDatCuoc = [...state.mangDatCuoc];
      cloneMAngDatCuoc = cloneMAngDatCuoc.map((item, index) => {
        if (item.ma === action.payload) {
          return { ...item, datCuoc: true };
        }
        return { ...item, datCuoc: false };
      });

      state.mangDatCuoc = cloneMAngDatCuoc;
      return { ...state };
    }
    case "RANDOM": {
      let soNgauNhien = Math.floor(Math.random() * 3);
      let quanCuocNgauNhien = state.mangDatCuoc[soNgauNhien];
      state.computer = quanCuocNgauNhien;
      return { ...state };
    }
    case "XU_LY_KET_QUA": {
      let player = state.mangDatCuoc.find((item) => item.datCuoc === true);
      let computer = state.computer;
      console.log("player", player.ma);
      console.log("computer", computer.ma);
      switch (player.ma) {
        case "keo":
          if (computer.ma === "bao") {
            state.soBanThang += 1;
            state.ketQua = "Thắng";
          } else if (computer.ma === "bua") {
            state.ketQua = "Thua";
          } else {
            state.ketQua = "Hòa";
          }
          break;

        case "bua":
          if (computer.ma === "keo") {
            state.soBanThang += 1;
            state.ketQua = "Thắng";
          } else if (computer.ma === "bua") {
            state.ketQua = "Hòa";
          } else {
            state.ketQua = "Thua";
          }
          break;
        case "bao":
          if (computer.ma === "keo") {
            state.ketQua = "Thua";
          } else if (computer.ma === "bao") {
            state.ketQua = "Hòa";
          } else {
            state.soBanThang += 1;
            state.ketQua = "Thắng";
          }
          break;
        default:
      }
      state.soBanChoi += 1;

      return { ...state };
    }
    default:
      return state;
  }
};

import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { rootReducer } from "./producer/rootProducer";


export const store = createStore(rootReducer,composeWithDevTools())
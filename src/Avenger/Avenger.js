import React, { Component } from "react";
import "./Avenger.css";
import Computer from "./Computer";
import KetQua from "./KetQua";
import Player from "./Player";
import { connect } from "react-redux";
class Avenger extends Component {
  render() {
    return (
      <div className="game">
        <div className="row text-center mt-5">
          <div className="col-4">
            <Player></Player>
          </div>

          <div className="col-4">
            <KetQua></KetQua>
            <button
              className="btn btn-danger"
              onClick={() => this.props.playGame()}
            >
              PlayGame
            </button>
          </div>
          <div className="col-4">
            <Computer></Computer>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProp = (dispatch) => {
  return {
    playGame: (params) => {
      // khai báo hàm lập đi lặp lại
      let count = 0;
      let randomComputer = setInterval((params) => {
        dispatch({
          type: "RANDOM",
        });
        count++;
        if (count > 10) {
          clearInterval(randomComputer);
          dispatch({
            type: "XU_LY_KET_QUA"
          })
        }
      }, 100);
    },
  };
};
export default connect(null, mapDispatchToProp)(Avenger);

import React, { Component } from "react";
import { connect } from "react-redux";

class Computer extends Component {
  render() {
    let keyFrame = `@keyframes randomComputer${Date.now()} {
      0% {top: -20px;}
      25% {top: 20px;}
      50% {top: -20px;}
      75% {top: 20px;}
      100% {top: 0px;}
    }`;

    return (
      <div className="playerRole">
        <style>{keyFrame}</style>
        <div className="thinking" style={{ position: "relative" }}>
          <img
            style={{
              position: "absolute",
              left: "7px",
              animation: `randomComputer${Date.now()} 0.5s`,
              transform: "rotate(125deg)",
            }}
            src={this.props.computer.hinhAnh}
            width={100}
            height={100}
            alt=""
          />
        </div>
        <div className="speech-bubble"></div>
        <img
          width={150}
          height={150}
          src="./imgg/playerComputer.png"
          alt="player"
        />
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    computer: state.avengerProducer.computer,
  };
};

export default connect(mapStateToProps, null)(Computer);

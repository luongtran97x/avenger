import React, { Component } from "react";
import { connect } from "react-redux";

class Player extends Component {
  render() {
    return (
      <div className="playerRole">
        <div className="thinking">
          <img src={this.props.mangDatCuoc.find(item => item.datCuoc === true).hinhAnh} width={100} height={100} alt="" />
        </div>
        <div className="speech-bubble"></div>
        <img width={150} height={150} src="./imgg/player.png" alt="player" />

        <div className="row">
          {this.props.mangDatCuoc.map((item, index) => {
            let border = {};
            if (item.datCuoc) {
              border = { border: "3px solid yellow" };
            }

            return (
              <div className="col-4" key={index}>
                <button style={border} className="btn btn-info btnItem" onClick={()=>this.props.datCuoc(item.ma)}>
                  <img src={item.hinhAnh} alt="keo" width={50} />
                </button>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    mangDatCuoc: state.avengerProducer.mangDatCuoc,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    datCuoc : (maCuoc) => {
      dispatch({
        type : "CHON_KEO_BUA_BAO",
        payload : maCuoc
      })
    }
    
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Player);
